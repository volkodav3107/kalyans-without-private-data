package ru.gooddocs.kalyans.UI;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by vlad on 28.01.17.
 */
@SpringUI
@SpringComponent
@Theme("valo")
public class MainUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        VerticalLayout verticalLayout = new VerticalLayout();
        Button button = new Button("Just button");
        button.addClickListener(event -> {
            Notification notification = new Notification("Click", Notification.Type.HUMANIZED_MESSAGE);
            notification.show(this.getPage());
        });
        verticalLayout.addComponent(button);
        setContent(verticalLayout);
    }
}
