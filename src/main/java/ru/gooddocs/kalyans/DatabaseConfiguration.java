package ru.gooddocs.kalyans;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad on 28.01.17.
 */
@Configuration
public class DatabaseConfiguration {

    private static final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

    @Bean
    @Primary
    public DriverManagerDataSource driverManagerDataSource() throws IOException {
        boolean stringIsLocal;
        stringIsLocal = (System.getProperty("user.home")).contains("vlad");
        log.error("DatabaseConfiguration","isLocal" + stringIsLocal);
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        if (stringIsLocal) {
            driverManagerDataSource.setUrl("jdbc:mysql://localhost/kalyans?autoReconnect=true&useSSL=false&characterEncoding=utf-8");
            driverManagerDataSource.setPassword("Here's may be my password");
        } else {
            driverManagerDataSource.setUrl("");
            driverManagerDataSource.setPassword("");
        }
        driverManagerDataSource.setUsername("root");
        return driverManagerDataSource;
    }


    @Bean
    public MongoClient mongo() throws IOException {
        boolean stringIsLocal;
        stringIsLocal = (System.getProperty("user.home")).contains("vlad");
        log.error("DatabaseConfiguration","isLocal" + stringIsLocal);
        if (stringIsLocal) {
            return new MongoClient("localhost",27017);
        } else {
            ServerAddress serverAddress = new ServerAddress("node122955-my-docs.jelastic.regruhosting.ru",27017);
            MongoCredential mongoCredential = MongoCredential.createCredential("admin","admin","Here's may be my password".toCharArray());
            List<MongoCredential> mongoCredentials = new ArrayList<>();
            mongoCredentials.add(mongoCredential);
            return new MongoClient(serverAddress, mongoCredentials);
        }
    }
}
