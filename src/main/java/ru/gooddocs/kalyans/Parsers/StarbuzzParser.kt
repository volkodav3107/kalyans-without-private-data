package ru.gooddocs.kalyans.Parsers

import javax.persistence.*

/**
 * Created by vlad on 16.07.17.
 */
@Entity
@Table(name = "Tobacco")
data class TobaccoEntity2(
        @Column(length = 250, nullable = false) private var categoryEn: String,
        @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "_id", length = 11, nullable = false)
        private var id: Int, @Column(length = 250, nullable = false)
        private var categoryRu: String, @Column(length = 4500, nullable = false)
        private var descriptionEn: String, @Column(length = 4500, nullable = false)
        private var descriptionRu: String
) {



    @Column(length = 250, nullable = false)
    private var nameEn: String? = null

    @Column(length = 250, nullable = false)
    private var nameRu: String? = null

    @Column(length = 250, nullable = false)
    private var nameProducer: String? = null

    /*   @Column(nullable = false)
    private Double price;
*/
    @Column(nullable = false)
    private var weight: String? = null

    @Column(nullable = false)
    private var image: String? = null

}

