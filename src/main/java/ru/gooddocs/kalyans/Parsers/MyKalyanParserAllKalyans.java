package ru.gooddocs.kalyans.Parsers;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.gooddocs.kalyans.database.sql.entities.TobaccoEntity;
import ru.gooddocs.kalyans.database.sql.interfaces.TobaccoRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser for AllKalyans
 * Created by vlad on 28.01.17.
 */
public class MyKalyanParserAllKalyans {


    private final static String pathToSite = "http://www.alkalyans.ru";
    private static List<String> nameElementsList = new ArrayList<>();
    private static List<String> priceElementsList = new ArrayList<>();
    private static List<String> tobaccoNameEn = new ArrayList<>();
    private static List<String> tobaccoNameRu = new ArrayList<>();
    private static List<String> tobaccoProducer = new ArrayList<>();
    private static List<String> tobaccoWeight = new ArrayList<>();
    private static List<String> tobaccoImage = new ArrayList<>();
    private static List<String> tobaccoImageToGetImage = new ArrayList<>();
    private static List<Double> tobaccoPrice = new ArrayList<>();
    private static List<URL> urlList = new ArrayList<>();

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

    }

    public static void doAll(TobaccoRepository tobaccoRepository) throws ParserConfigurationException, SAXException, IOException {
        urlList.add(new URL(pathToSite + "/catalog/adalya/"));
        urlList.add(new URL(pathToSite + "/catalog/afzal/"));
        urlList.add(new URL(pathToSite + "/catalog/al_fakhamah/"));
        urlList.add(new URL(pathToSite + "/catalog/al_fakher/"));
        urlList.add(new URL(pathToSite + "/catalog/al_waha/"));
        urlList.add(new URL(pathToSite + "/catalog/alchemist_blend/"));
        urlList.add(new URL(pathToSite + "/catalog/argelini/"));
        urlList.add(new URL(pathToSite + "/catalog/dark_side/"));
        urlList.add(new URL(pathToSite + "/catalog/doobacco/"));
        urlList.add(new URL(pathToSite + "/catalog/fasil/"));
        urlList.add(new URL(pathToSite + "/catalog/fumari/"));
        urlList.add(new URL(pathToSite + "/catalog/hookafina/"));
        urlList.add(new URL(pathToSite + "/catalog/khalil_maamoon/"));
        urlList.add(new URL(pathToSite + "/catalog/layalina/"));
        urlList.add(new URL(pathToSite + "/catalog/nakhla/"));
        urlList.add(new URL(pathToSite + "/catalog/nirvana/"));
        urlList.add(new URL(pathToSite + "/catalog/serbetli/"));
        urlList.add(new URL(pathToSite + "/catalog/social_smoke_tobacco/"));
        urlList.add(new URL(pathToSite + "/catalog/starbuzz/"));
        urlList.add(new URL(pathToSite + "/catalog/tangiers/"));
        checkURL();

        for (URL anUrlList : urlList) {
            Parse(anUrlList);
        }
        parseName();
        parcePrice();
        System.out.println(
                " tobaccoNameRu " + tobaccoNameRu.size() +
                        "\ttobaccoNameEn" + tobaccoNameEn.size() +
                        "\ttobaccoWeight" + tobaccoWeight.size() +
                        "\ttobaccoProducer" + tobaccoProducer.size() +
                        "\ttobaccoPrice" + tobaccoPrice.size());

        for (int i = 0; i < nameElementsList.size()-1; i++) {
            TobaccoEntity tobacco = new TobaccoEntity(tobaccoNameEn.get(i),
                    tobaccoNameRu.get(i),
                    tobaccoProducer.get(i),
                    tobaccoPrice.get(i),
                    tobaccoWeight.get(i),
                    tobaccoImage.get(i)
            );
            System.out.println(tobacco.toString());
        }

//      loadImages();
        writeToDataBase(tobaccoRepository);
    }

    private static void writeToDataBase(TobaccoRepository tobaccoRepository) {
        System.out.println(tobaccoNameEn.size() + "\t"
                + tobaccoNameRu.size() + "\t"
                + tobaccoProducer.size() + "\t"
                + tobaccoWeight.size() + "\t"
                + tobaccoPrice.size() + "\t");

        for (int i = 0; i < nameElementsList.size(); i++) {
            TobaccoEntity tobacco = new TobaccoEntity(tobaccoNameEn.get(i),
                    tobaccoNameRu.get(i),
                    tobaccoProducer.get(i),
                    tobaccoPrice.get(i),
                    tobaccoWeight.get(i),
                    tobaccoImage.get(i)
            );
            tobaccoRepository.save(tobacco);

        }
    }


    private static void loadImages() throws IOException {
        String s;
        URL url;
        HttpURLConnection urlConnection;
        InputStream inputStream;
        File file1mage;
        for (String aTobaccoImageToGetImage : tobaccoImageToGetImage) {
            s = aTobaccoImageToGetImage;
            if (!s.equals("")) {
                System.out.println(s);
                url = new URL(s);
                urlConnection = (HttpURLConnection) url.openConnection();
                inputStream = urlConnection.getInputStream();
                s = s.substring(s.lastIndexOf("/") + 1);
                file1mage = new File("/home/vlad/IdeaProjects/kalyans/src/main/resources/images/" + s);
                OutputStream outputStream = new FileOutputStream(file1mage);
                byte[] bytes = new byte[1024];
                int bytesReads;
                while ((bytesReads = inputStream.read(bytes)) > 0) {
                    outputStream.write(bytes, 0, bytesReads);
                }
                outputStream.close();
                inputStream.close();
            }

        }
    }


    private static void parseName() {
        String s;
        String stringPatten;
        String tempNameRu = "";
        String tempNameEn = "";
        String tempProducer = "";
        String tempWeight = "";
        Pattern pattern;
        Matcher matcher;
        for (String aNameElementsList : nameElementsList) {
            s = aNameElementsList;
            s = s.replace("Эль Накхла", "Nakhla");
            System.out.println("Start \t" + s);

            s = s.replace("&laquo;", "");
            s = s.replace("&raquo;", "");
            System.out.println("This is replace\t" + s);

            stringPatten = "[0-9]{1,5} [кгр]+";
            pattern = Pattern.compile(stringPatten);
            matcher = pattern.matcher(s);
            if (matcher.find()) {
                tempWeight = s.substring(matcher.start(), matcher.end());
                s = s.replace(tempWeight, "");
                System.out.println("after tempWeight\t" + s);
            }

            stringPatten = "Табак [a-zA-Z ]*+";
            pattern = Pattern.compile(stringPatten);
            matcher = pattern.matcher(s);
            if (matcher.find()) {
                tempProducer = s.substring(matcher.start() + 6, matcher.end() - 1);
                s = s.replace(tempProducer, "");
                while (tempProducer.lastIndexOf(" ") == tempProducer.length() - 1) {
                    tempProducer = tempProducer.substring(0, tempProducer.length() - 1);
                }
                s = s.replace("Табак", "");
                System.out.println("after tempProducer\t" + s);
            }

            stringPatten = "[a-zA-Z].*[a-zA-Z]+";
            pattern = Pattern.compile(stringPatten);
            matcher = pattern.matcher(s);
            if (matcher.find()) {
                tempNameEn = s.substring(matcher.start(), matcher.end());
                s = s.replace(tempNameEn, "");
                System.out.println("after tempNameEn\t" + s);
            }

            stringPatten = "[а-яА-Я].*[а-яА-Я]+";
            pattern = Pattern.compile(stringPatten);
            matcher = pattern.matcher(s);
            if (matcher.find()) {
                tempNameRu = s.substring(matcher.start(), matcher.end());
//            System.out.println(tempNameRu);
            }
            tobaccoProducer.add(tempProducer);
            tobaccoNameEn.add(tempNameEn);
            tobaccoNameRu.add(tempNameRu);
            tobaccoWeight.add(tempWeight);
            tempNameRu = "";
            tempNameEn = "";
            tempProducer = "";
            tempWeight = "";
        }
    }


    private static void parcePrice() {
        for (String aPriceElementsList : priceElementsList) {
            String s = aPriceElementsList.replace(" ", "");
            tobaccoPrice.add(Double.parseDouble(s));
        }
    }


    /**
     * Метод для получения всех ссылок в категории
     *
     */
    private static void checkURL() throws IOException, ParserConfigurationException, SAXException {
        String outString = "";
        boolean needCheck = true;
        int i = 0;
        while (needCheck) {
            CleanerProperties properties = new CleanerProperties();
            properties.setTranslateSpecialEntities(true);
            properties.setTransResCharsToNCR(true);
            properties.setOmitComments(true);
            TagNode tagNode = new HtmlCleaner(properties).clean(urlList.get(i));
            DomSerializer domSerializer = new DomSerializer(properties);
            Document document = domSerializer.createDOM(tagNode);
            NodeList nodeList = document.getElementsByTagName("div");
            for (int j = 0; j < nodeList.getLength(); j++) {
                Element element = (Element) nodeList.item(j);
                if (element.getAttribute("class").equals("catalog-pagenav")) {
                    nodeList = element.getElementsByTagName("a");
                    element = (Element) nodeList.item(0);
                    outString = element.getAttribute("href");
                    break;
                }
            }
            if (outString.equals("") || outString.equals("null")) {
                needCheck = false;
            } else {
                urlList.add(new URL(pathToSite + outString));
                i++;
                outString = "";
            }
        }
        printListURL(urlList);
    }


    /**
     * Основной метод парсинга
     *
     * @param url адрес сайта (до категории)
     */
    private static void Parse(URL url) throws IOException, ParserConfigurationException, SAXException {
        CleanerProperties props = new CleanerProperties();

// set some properties to non-default values
        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
// do parsing
        TagNode tagNode = new HtmlCleaner(props).clean(url);

// serialize to xml file
        DomSerializer domSerializer = new DomSerializer(new CleanerProperties());

        Document document = domSerializer.createDOM(tagNode);
        NodeList nodeList = document.getElementsByTagName("div");

        NodeList realCatalogNodeList = null;

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            if (element.getAttribute("class").equals("catalog-page")) {
                realCatalogNodeList = element.getElementsByTagName("div");
            }
        }

        for (int i = 0; i < (realCatalogNodeList != null ? realCatalogNodeList.getLength() : 0); i++) {
            Element element = (Element) realCatalogNodeList.item(i);
            if (element.getAttribute("class").equals("name")) {
                nameElementsList.add(element.getElementsByTagName("a").item(0).getTextContent());
            } else if (element.getAttribute("class").equals("price")) {
                priceElementsList.add(element.getElementsByTagName("span").item(0).getTextContent());
            } else if (element.getAttribute("class").equals("in")) {
                if (element.getElementsByTagName("a").getLength() > 0) {
                    String s = element.getElementsByTagName("a").item(0).getAttributes().getNamedItem("href").getNodeValue();
                    tobaccoImageToGetImage.add(pathToSite + s);
                    s = s.substring(s.lastIndexOf("/") + 1);
                    tobaccoImage.add(s);
                } else {
                    tobaccoImage.add("");
                    tobaccoImageToGetImage.add("");
                }

            }

        }
    }

    private static void printListString(List<String> elements) {
        for (String element : elements) {
            System.out.println(element);
        }
    }

    private static void printListURL(List<URL> elements) {
        for (URL element : elements) {
            System.out.println(element.toString());
        }
    }

    /**
     * Метод для тестов
     *
     * @param s адрес сайта
     */
    public static void getResult(String s) throws ParserConfigurationException, SAXException, IOException {
        urlList.add(new URL(s));
        checkURL();
        for (URL anUrlList : MyKalyanParserAllKalyans.urlList) {
            Parse(anUrlList);
        }
        System.out.println("\n\nName\n\n");
        printListString(nameElementsList);
        System.out.println("\n\nPrice\n\n");
        printListString(priceElementsList);
    }
}

