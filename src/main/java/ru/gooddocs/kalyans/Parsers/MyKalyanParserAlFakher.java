package ru.gooddocs.kalyans.Parsers;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.gooddocs.kalyans.database.sql.entities.TobaccoEntity;
import ru.gooddocs.kalyans.database.sql.interfaces.TobaccoRepository;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vlad on 28.01.17.
 */
public class MyKalyanParserAlFakher {

    static List<TobaccoEntity> tobacco2List = new ArrayList<TobaccoEntity>();
    static List<URL> urlList = new ArrayList<URL>();
    final static String rangePathToSite = "http://www.alfakher.com/our-range/";
    final static String globalPathToSite = "http://www.alfakher.com/";
    static List<AlFakherTobacco> alFakherTobaccos = new ArrayList<AlFakherTobacco>();
    static List<String> stringsImagesToLoad = new ArrayList<String>();
    static List<List<String>> listsImagesLists = new ArrayList<>();
    static int counterList = 0;

    private static HashMap<Integer, String> hashMapCategoryEn = new HashMap<>();
    private static HashMap<String, String> hashMapCategoryFromEnToRu = new HashMap<>();
    private static TobaccoRepository tobaccoRepository;

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {


    }

    public static void startFromCommandLiner(TobaccoRepository repository) throws IOException, SAXException, ParserConfigurationException {
        hashMapCategoryFromEnToRu.put("STANDARD RANGE","Стандартная коллекция");
        hashMapCategoryFromEnToRu.put("GOLDEN RANGE","Золотая линейка");
        hashMapCategoryFromEnToRu.put("SPECIAL EDITION","Специальные товары");
        hashMapCategoryFromEnToRu.put("NON-TOBACCO","Нетабачная продукция");
        tobaccoRepository = repository;
        doAll();

    }

    /**
     * Main method which i can use from other class
     *
     */
    private static void doAll() throws ParserConfigurationException, SAXException, IOException {
        URL myUrl = new URL(rangePathToSite);
        URLConnection urlConnEn = myUrl.openConnection();
        urlConnEn.connect();
        InputStream inputStream = urlConnEn.getInputStream();

        checkURL(inputStream);
        ParseCategory(false);
        for (URL anUrlList : urlList) {
            ParseEnglish(anUrlList);
        }
        urlList.clear();
        URLConnection urlConnRu = new URL(rangePathToSite).openConnection();
        String myCookie = "lang=Pусский";
        urlConnRu.setRequestProperty("Cookie", myCookie);
        urlConnRu.connect();
        InputStream inputStreamRu = urlConnRu.getInputStream();
        checkURL(inputStreamRu);

        for (int i = 0; i < urlList.size(); i++) {
            urlConnRu = urlList.get(i).openConnection();
            urlConnRu.setRequestProperty("Cookie", myCookie);
            urlConnRu.connect();
            inputStreamRu = urlConnRu.getInputStream();
            ParseRussian(inputStreamRu, i);
        }


        int i = 0;
        List<String> stringList = new ArrayList<>();
        for (String s :
                stringsImagesToLoad) {
            if (i<10){
                stringList.add(s);
                i++;
            } else {
                listsImagesLists.add(stringList);
                i = 0;
                counterList++;
                stringList = new ArrayList<>();
                stringList.add(s);
            }
        }

        System.out.println("runnable");
        Runnable runnable = () -> {
            try {
                int i1 = counterList;
                loadImages(listsImagesLists.get(i1));
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        while (counterList > 0){
            counterList--;
            runnable.run();
        }
        System.out.println("runnable starts");
//        printListString(alFakherTobaccos);
        AlFakherTobacco.toTobacco2(alFakherTobaccos);
        writeToDataBase();
    }

    /**
     * Write all AlFakherTobacco to DataBase
     */
    private static void writeToDataBase() {
        for (TobaccoEntity tobacco : tobacco2List) {
            tobaccoRepository.save(tobacco);
        }
    }


    /**
     * Load all images of alFakherTobacco
     * and change path to image in AlFakherTobacco to local path
     ** @throws IOException
     */
    private static void loadImages(List<String> stringsImages) throws IOException {
        File file = new File("//home/vlad/IdeaProjects/kalyans/src/main/resources/images/alFakher/");
        file.mkdirs();
        URL url;
        HttpURLConnection urlConnection;
        InputStream inputStream;
        File file1mage;
        for (String imagePathString : stringsImages) {
            if (!imagePathString.equals("")) {
//                    System.out.println(s);
                url = new URL(imagePathString);
                urlConnection = (HttpURLConnection) url.openConnection();
                inputStream = urlConnection.getInputStream();
                imagePathString = imagePathString.substring(imagePathString.lastIndexOf("/") + 1);
//                    System.out.println(imagePathString);
                file1mage = new File(file.getAbsoluteFile() + "/" + imagePathString);
                if (!file1mage.exists()) {
                    OutputStream outputStream = new FileOutputStream(file1mage);
                    byte[] bytes = new byte[1024];
                    int bytesReads;
                    while ((bytesReads = inputStream.read(bytes)) > 0) {
                        outputStream.write(bytes, 0, bytesReads);
                    }
                    outputStream.close();
                }
                inputStream.close();
            }
        }

    }


    /**
     * Get all links from site's categore
     *
     * @param inputStream inputStream from URL with Cache
     */
    private static void checkURL(InputStream inputStream) throws IOException, ParserConfigurationException, SAXException {
        CleanerProperties properties = new CleanerProperties();
        properties.setTranslateSpecialEntities(true);
        properties.setTransResCharsToNCR(true);
        properties.setOmitComments(true);
        TagNode tagNode = new HtmlCleaner(properties).clean(inputStream);
        DomSerializer domSerializer = new DomSerializer(properties);
        Document document = domSerializer.createDOM(tagNode);
        NodeList nodeList = document.getElementsByTagName("a");
        Node node;
        for (int j = 0; j < nodeList.getLength(); j++) {
            node = nodeList.item(j);
            if (node.getAttributes().getNamedItem("id") != null) {
                Element element = (Element) node;
                if (Integer.parseInt(element.getAttribute("id").replace("prodid", "")) < 131) {
//                    System.out.println(node.getTextContent());
                    urlList.add(new URL(globalPathToSite + element.getAttribute("href")));
                }
            }
        }

//        printListURL(urlList);

    }


    /**
     * Parsing site with English text
     *
     * @param url url of site (with category)
     */
    private static void ParseEnglish(URL url) throws IOException, ParserConfigurationException, SAXException {

        AlFakherTobacco alFakherTobacco = new AlFakherTobacco();
        CleanerProperties props = new CleanerProperties();

// set some properties to non-default values
        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
// do parsing
        TagNode tagNode = new HtmlCleaner(props).clean(url);

// serialize to xml file
        DomSerializer domSerializer = new DomSerializer(new CleanerProperties());

        Document document = domSerializer.createDOM(tagNode);
        NodeList nodeList = document.getElementsByTagName("div");

        NodeList nodeListImg = document.getElementsByTagName("img");
        NodeList nodeListId = document.getElementsByTagName("input");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            if (element.getAttribute("class").equals("product_title")) {
                alFakherTobacco.setAlFakherTobaccoNameEn(html2text(element.getTextContent()));
            } else if (element.getAttribute("class").equals("product_description")) {
                alFakherTobacco.setAlFakherTobaccoDescriptionEn(html2text(element.getTextContent()));
            } else if (element.getAttribute("class").contains("fl quantity_title")) {
                boolean addingBool = false;
                String stringPattern = "[0-9]{1,4}+";
                String stringNow = element.getTextContent();
                if (stringNow.contains("carton")) {
                    addingBool = true;
                }
                Pattern pattern = Pattern.compile(stringPattern);
                Matcher matcher = pattern.matcher(stringNow);
                if (matcher.find()) {
                    stringNow = stringNow.substring(matcher.start(), matcher.end());
                }
                if (addingBool) {
                    stringNow = stringNow + "0";
                }
                alFakherTobacco.getAlFakherTobaccoWeight().add(stringNow);
            }
        }

        for (int i = 0; i < nodeListImg.getLength(); i++) {
            Element element = (Element) nodeListImg.item(i);
            if (element.getAttribute("class").contains("prod_img")) {
                String s = element.getAttribute("src");
                stringsImagesToLoad.add(globalPathToSite + s);
                alFakherTobacco.getAlFakherTobaccoImage().add("alFakher/" + s.substring(s.lastIndexOf("/") + 1));
            }
        }

        Integer integerId = 0;
        for (int i = 0; i < nodeListId.getLength(); i++) {
            Element element = (Element) nodeListId.item(i);
            if (element.getAttribute("name").equals("prod")) {
                integerId = Integer.parseInt(element.getAttribute("value"));
            }
        }

        alFakherTobacco.setAlFakherTobaccoCategoryEn(hashMapCategoryEn.get(integerId));
        alFakherTobacco.setAlFakherTobaccoCategoryRu(hashMapCategoryFromEnToRu.get(hashMapCategoryEn.get(integerId)));
        alFakherTobacco.setAlFakherTobaccoProducer("Al Fakher");
        alFakherTobaccos.add(alFakherTobacco);
    }


    /**
     * Parsing sites with Russian text
     *
     * @param inputStream inputStream from URL with cache
     * @param counter     counter which can show us what element of alFakherTobaccos we edit now
     */
    private static void ParseRussian(InputStream inputStream, int counter) throws IOException, ParserConfigurationException, SAXException {
//        System.out.println(inputStream);
        CleanerProperties props = new CleanerProperties();
// set some properties to non-default values
        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
// do parsing
        TagNode tagNode = new HtmlCleaner(props).clean(inputStream);
// serialize to xml file
        DomSerializer domSerializer = new DomSerializer(new CleanerProperties());
        Document document = domSerializer.createDOM(tagNode);
//        System.out.println(document.);
        NodeList nodeList = document.getElementsByTagName("div");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            if (element.getAttribute("class").equals("product_title")) {
                alFakherTobaccos.get(counter).setAlFakherTobaccoNameRu(html2text(element.getTextContent()));
            } else if (element.getAttribute("class").equals("product_description")) {
                alFakherTobaccos.get(counter).setAlFakherTobaccoDescriptionRu(html2text(element.getTextContent()));
            }
        }

    }


    private static void ParseCategory(boolean itIsEnglish) throws IOException, ParserConfigurationException, SAXException {
        String nameCategory;
        Integer nowIdProduct;

        URLConnection urlConnRu = new URL(rangePathToSite).openConnection();
        if (itIsEnglish) {
            String myCookie = "lang=Pусский";
            urlConnRu.setRequestProperty("Cookie", myCookie);
        }
        urlConnRu.connect();
        InputStream inputStream = urlConnRu.getInputStream();
        CleanerProperties props = new CleanerProperties();
// set some properties to non-default values
        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
// do parsing
        TagNode tagNode = new HtmlCleaner(props).clean(inputStream);
        tagNode = tagNode.getChildTagList().get(1).getChildTagList().get(1).getChildTagList().
                get(2).getChildTagList().get(0).getChildTagList().get(0);
        for (TagNode tagNode1 : tagNode.getChildTagList()) {
            if (tagNode1.getAttributes().containsValue("categ")) {
                nameCategory = tagNode1.getChildTagList().get(0).getText().toString();
                System.out.println("nameCategory = " + nameCategory);
                for (TagNode tagNode2 : tagNode1.getChildTagList().get(1).getChildTagList()) {
                    if (!tagNode2.getAttributes().containsValue("clear")) {
//                        System.out.println("tagNode2.getChildTagList() " + tagNode2.getChildTagList());
                        for (TagNode tagNode3 : tagNode2.getChildTagList()) {
                            if (tagNode3.getName().equals("a")) {
                                nowIdProduct = Integer.parseInt(tagNode3.getAttributes().get("id").replace("prodid", ""));
//                                System.out.println("id = " + nowIdProduct);
                                hashMapCategoryEn.put(nowIdProduct, nameCategory);
                            } else {
                                if (tagNode3.getAttributes().containsValue("categ2_container fl")) {
                                    for (TagNode tagNode4 : tagNode3.getChildTagList()) {
                                        nowIdProduct = Integer.parseInt(tagNode4.getAttributes().get("id").replace("prodid", ""));
//                                        System.out.println("id = " + nowIdProduct);
                                        hashMapCategoryEn.put(nowIdProduct, nameCategory);
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

    }


    /**
     * Print List of AlFakherTobaccos
     *
     * @param elements List of AlFakherTobacco which will print
     */
    private static void printListString(List<AlFakherTobacco> elements) {
        for (AlFakherTobacco element : elements) {
            System.out.println(element);
        }
    }


    /**
     * @param html String which contains html tags
     * @return String without html tag's, clean text
     */
    private static String html2text(String html) {
        return Jsoup.parse(html).text();
    }


    /**
     * Class for AlFakher Tobacco different with Tobacco2,because can have
     * two or more weight and images in one tobacco
     */
    private static class AlFakherTobacco {
        private String alFakherTobaccoCategoryEn;
        private String alFakherTobaccoCategoryRu;
        private String alFakherTobaccoDescriptionEn;
        private String alFakherTobaccoDescriptionRu;
        private String alFakherTobaccoNameEn;
        private String alFakherTobaccoNameRu;
        private String alFakherTobaccoProducer;
        private LinkedHashSet<String> alFakherTobaccoWeight;
        private List<String> alFakherTobaccoImage = new ArrayList<>();


        public AlFakherTobacco() {
            alFakherTobaccoWeight = new LinkedHashSet<>();
        }


        public String getAlFakherTobaccoCategoryRu() {
            return alFakherTobaccoCategoryRu;
        }

        public void setAlFakherTobaccoCategoryRu(String alFakherTobaccoCategoryRu) {
            this.alFakherTobaccoCategoryRu = alFakherTobaccoCategoryRu;
        }

        public String getAlFakherTobaccoCategoryEn() {
            return alFakherTobaccoCategoryEn;
        }

        public void setAlFakherTobaccoCategoryEn(String alFakherTobaccoCategoryEn) {
            this.alFakherTobaccoCategoryEn = alFakherTobaccoCategoryEn;
        }

        public String getAlFakherTobaccoDescriptionEn() {
            return alFakherTobaccoDescriptionEn;
        }

        public void setAlFakherTobaccoDescriptionEn(String alFakherTobaccoDescriptionEn) {
            this.alFakherTobaccoDescriptionEn = alFakherTobaccoDescriptionEn;
        }

        public String getAlFakherTobaccoDescriptionRu() {
            return alFakherTobaccoDescriptionRu;
        }

        public void setAlFakherTobaccoDescriptionRu(String alFakherTobaccoDescriptionRu) {
            this.alFakherTobaccoDescriptionRu = alFakherTobaccoDescriptionRu;
        }

        public String getAlFakherTobaccoNameEn() {
            return alFakherTobaccoNameEn;
        }

        public void setAlFakherTobaccoNameEn(String alFakherTobaccoNameEn) {
            this.alFakherTobaccoNameEn = alFakherTobaccoNameEn;
        }

        public String getAlFakherTobaccoNameRu() {
            return alFakherTobaccoNameRu;
        }

        public void setAlFakherTobaccoNameRu(String alFakherTobaccoNameRu) {
            this.alFakherTobaccoNameRu = alFakherTobaccoNameRu;
        }

        public String getAlFakherTobaccoProducer() {
            return alFakherTobaccoProducer;
        }

        public void setAlFakherTobaccoProducer(String alFakherTobaccoProducer) {
            this.alFakherTobaccoProducer = alFakherTobaccoProducer;
        }

        public LinkedHashSet<String> getAlFakherTobaccoWeight() {
            return alFakherTobaccoWeight;
        }

        public List<String> getAlFakherTobaccoImage() {
            return alFakherTobaccoImage;
        }


        @Override
        public String toString() {
            return "AlFakherTobacco{" +
                    "alFakherTobaccoCategoryEn='" + alFakherTobaccoCategoryEn + '\'' +
                    ", alFakherTobaccoCategoryRu='" + alFakherTobaccoCategoryRu + '\'' +
                    ", alFakherTobaccoDescriptionEn='" + alFakherTobaccoDescriptionEn + '\'' +
                    ", alFakherTobaccoDescriptionRu='" + alFakherTobaccoDescriptionRu + '\'' +
                    ", alFakherTobaccoNameEn='" + alFakherTobaccoNameEn + '\'' +
                    ", alFakherTobaccoNameRu='" + alFakherTobaccoNameRu + '\'' +
                    ", alFakherTobaccoProducer='" + alFakherTobaccoProducer + '\'' +
                    ", alFakherTobaccoWeight=" + alFakherTobaccoWeight +
                    ", alFakherTobaccoImage=" + alFakherTobaccoImage +
                    '}';
        }


        public static void toTobacco2(List<AlFakherTobacco> fakherTobaccoList) {

            for (AlFakherTobacco aFakherTobaccoList : fakherTobaccoList) {
                int j = 0;
                AlFakherTobacco tobacco = aFakherTobaccoList;
                for (Object o : tobacco.getAlFakherTobaccoWeight()) {
                    if (tobacco.getAlFakherTobaccoCategoryRu() == null){
                        System.out.println(tobacco2List.toString());
                    }
                    TobaccoEntity tobacco2 = new TobaccoEntity(tobacco.getAlFakherTobaccoCategoryEn(),
                            tobacco.getAlFakherTobaccoCategoryRu(),
                            tobacco.getAlFakherTobaccoDescriptionEn(),
                            tobacco.getAlFakherTobaccoDescriptionRu(),
                            tobacco.getAlFakherTobaccoNameEn(),
                            tobacco.getAlFakherTobaccoNameRu(),
                            tobacco.getAlFakherTobaccoProducer(),
                            (String) o,
                            tobacco.getAlFakherTobaccoImage().get(j),
                            0.0);

                    System.out.println(tobacco2.toString());
                    tobacco2List.add(tobacco2);
                    j++;
                }
            }

        }


    }
}
