package ru.gooddocs.kalyans.database.sql.entities;

import javax.persistence.*;

/**
 * Created by vlad on 28.01.17.
 */
@Entity
@Table(name = "Tobacco")
public class TobaccoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id", length = 11, nullable = false)
    private Integer id;

    @Column(length = 250,nullable = false)
    private String categoryEn;

    @Column(length = 250,nullable = false)
    private String categoryRu;

    @Column(length = 4500,nullable = false)
    private String descriptionEn;
    @Column(length = 4500,nullable = false)
    private String descriptionRu;



    @Column(length = 250, nullable = false)
    private String nameEn;

    @Column(length = 250, nullable = false)
    private String nameRu;

    @Column(length = 250, nullable = false)
    private String nameProducer;

 /*   @Column(nullable = false)
    private Double price;
*/
    @Column(nullable = false)
    private String weight;

    @Column(nullable = false)
    private String image;




    /**
     *
     * @param categoryEn
     * @param categoryRu
     * @param descriptionEn
     * @param descriptionRu
     * @param nameEn
     * @param nameRu
     * @param nameProducer
     * @param weight
     * @param image
     * @param price
     */
    public TobaccoEntity(String categoryEn, String categoryRu,
                         String descriptionEn, String descriptionRu,
                         String nameEn, String nameRu,
                         String nameProducer, String weight, String image, double price) {
        this.categoryEn = categoryEn;
        this.categoryRu = categoryRu;
        this.descriptionEn = descriptionEn;
        this.descriptionRu = descriptionRu;
        this.nameEn = nameEn;
        this.nameRu = nameRu;
        this.nameProducer = nameProducer;
        this.weight = weight;
        this.image = image;
//        this.price = price;
    }

    public TobaccoEntity(String nameEn, String nameRu, String nameProducer, Double price, String weight, String image) {
        this.nameEn = nameEn;
        this.nameRu = nameRu;
        this.nameProducer = nameProducer;
//        this.price = price;
        this.weight = weight;
        this.image = image;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryEn() {
        return categoryEn;
    }

    public void setCategoryEn(String categoryEn) {
        this.categoryEn = categoryEn;
    }

    public String getCategoryRu() {
        return categoryRu;
    }

    public void setCategoryRu(String categoryRu) {
        this.categoryRu = categoryRu;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameProducer() {
        return nameProducer;
    }

    public void setNameProducer(String nameProducer) {
        this.nameProducer = nameProducer;
    }

/*
public Double getPrice() {
return price;
}

public void setPrice(Double price) {
this.price = price;
}
*/

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "TobaccoEntity{" +
                "id=" + id +
                ", categoryEn='" + categoryEn + '\'' +
                ", categoryRu='" + categoryRu + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                ", descriptionRu='" + descriptionRu + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", nameRu='" + nameRu + '\'' +
                ", nameProducer='" + nameProducer + '\'' +
//                ", price=" + price +
                ", weight='" + weight + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
