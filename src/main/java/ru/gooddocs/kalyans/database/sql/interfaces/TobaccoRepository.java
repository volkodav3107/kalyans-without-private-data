package ru.gooddocs.kalyans.database.sql.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.gooddocs.kalyans.database.sql.entities.TobaccoEntity;

/**
 * Created by vlad on 28.01.17.
 */
public interface TobaccoRepository extends JpaRepository<TobaccoEntity,Integer> {

}
