package ru.gooddocs.kalyans;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.gooddocs.kalyans.Parsers.MyKalyanParserAllKalyans;
import ru.gooddocs.kalyans.database.sql.interfaces.TobaccoRepository;

@SpringBootApplication
public class KalyansApplication {

    public static void main(String[] args) {
        SpringApplication.run(KalyansApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(TobaccoRepository tobaccoRepository) {
        return (args -> {
            MyKalyanParserAllKalyans.doAll(tobaccoRepository);
//            MyKalyanParserAlFakher.startFromCommandLiner(tobaccoRepository);
          /*  TobaccoEntity tobaccoEntity = new TobaccoEntity("catEn", "catRu",
                    "desrEn", "desrRu", "nameEn", "nameRu",
                    "nameProducer", "100", "none", 125.11);
            tobaccoRepository.save(tobaccoEntity);*/
            /*List<TobaccoEntity> tobaccoEntities = new ArrayList<>();
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "The strong flavour of black tobacco without it burning your throat. Soft Black is our original take on a traditional taste making black molasses smoother to smoke while keeping the full flavour experience.", "Крепкий аромат черного табака, не обжигающий горло. Мягкая черная смесь – наша интерпретация традиционного вкуса. Черная смесь имеет более мягкий аромат, но вместе с тем сохраняет богатый вкус.", "Soft Black", "Мягкая черная смесь", "Al Fakher", "1000", "alFakher/Soft_Black_1KG_Pack_copy.png", 112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "A sweet taste with an electric kick. Our energy drink molasses will energize your evening.", "Сладкий аромат с зарядом бодрости. Эта смесь будет заряжать вас энергией в течение всего вечера.", "Energy Drink", "Энергетический напиток", "Al Fakher", "1000", "alFakher/Energydrink_1KG_Pack_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "A sweet taste with an electric kick. Our energy drink molasses will energize your evening.", "Сладкий аромат с зарядом бодрости. Эта смесь будет заряжать вас энергией в течение всего вечера.", "Energy Drink", "Энергетический напиток", "Al Fakher", "250", "alFakher/Energy_Drink_250g_Pack_NEW_01_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "A sweet taste with an electric kick. Our energy drink molasses will energize your evening.", "Сладкий аромат с зарядом бодрости. Эта смесь будет заряжать вас энергией в течение всего вечера.", "Energy Drink", "Энергетический напиток", "Al Fakher", "500", "alFakher/R2_50g_Carton_Energy_Drink_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "A sweet taste with an electric kick. Our energy drink molasses will energize your evening.", "Сладкий аромат с зарядом бодрости. Эта смесь будет заряжать вас энергией в течение всего вечера.", "Energy Drink", "Энергетический напиток", "Al Fakher", "50", "alFakher/R2_50g_Pack_Energy_Drink_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Al Fakher cappuccino is infused with the finest coffee bean flavours. We’ve worked hard to reproduce the coffee shop experience. You can even taste the foam in our molasses.", "Смесь Al Fakher 'капуччино' с ароматом отборных кофейных бобов. Мы кропотливо старались воссоздать ароматную атмосферу кофейни. При курении смеси вы почувствуете даже вкус кофейной пены.", "Cappuccino", "Капуччино", "Al Fakher", "1000", "alFakher/Cappuccino_1KG_Pack_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Al Fakher cappuccino is infused with the finest coffee bean flavours. We’ve worked hard to reproduce the coffee shop experience. You can even taste the foam in our molasses.", "Смесь Al Fakher 'капуччино' с ароматом отборных кофейных бобов. Мы кропотливо старались воссоздать ароматную атмосферу кофейни. При курении смеси вы почувствуете даже вкус кофейной пены.", "Cappuccino", "Капуччино", "Al Fakher", "250", "alFakher/Cappuccino_250g_Pack_NEW_01_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Al Fakher cappuccino is infused with the finest coffee bean flavours. We’ve worked hard to reproduce the coffee shop experience. You can even taste the foam in our molasses.", "Смесь Al Fakher 'капуччино' с ароматом отборных кофейных бобов. Мы кропотливо старались воссоздать ароматную атмосферу кофейни. При курении смеси вы почувствуете даже вкус кофейной пены.", "Cappuccino", "Капуччино", "Al Fakher", "500", "alFakher/R2_50g_Carton_Cappuccino_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Al Fakher cappuccino is infused with the finest coffee bean flavours. We’ve worked hard to reproduce the coffee shop experience. You can even taste the foam in our molasses.", "Смесь Al Fakher 'капуччино' с ароматом отборных кофейных бобов. Мы кропотливо старались воссоздать ароматную атмосферу кофейни. При курении смеси вы почувствуете даже вкус кофейной пены.", "Cappuccino", "Капуччино", "Al Fakher", "50", "alFakher/R2_50g_Pack_Cappuccino_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Chocolate lovers beware; we've managed to reproduce the enticing palate of fine chocolate in our molasses. The powerful taste of cocoa with a beautiful creaminess.", "Любители шоколада, внимание! В нашей смеси мы постарались воссоздать восхитительный вкус роскошного шоколада. Яркий аромат какао и мягкие сливочные ноты.", "Chocolate", "Шоколад", "Al Fakher", "1000", "alFakher/Chocolate_1KG_Pack_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Chocolate lovers beware; we've managed to reproduce the enticing palate of fine chocolate in our molasses. The powerful taste of cocoa with a beautiful creaminess.", "Любители шоколада, внимание! В нашей смеси мы постарались воссоздать восхитительный вкус роскошного шоколада. Яркий аромат какао и мягкие сливочные ноты.", "Chocolate", "Шоколад", "Al Fakher", "250", "alFakher/Chocolate_250g_Pack_NEW_01_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Chocolate lovers beware; we've managed to reproduce the enticing palate of fine chocolate in our molasses. The powerful taste of cocoa with a beautiful creaminess.", "Любители шоколада, внимание! В нашей смеси мы постарались воссоздать восхитительный вкус роскошного шоколада. Яркий аромат какао и мягкие сливочные ноты.", "Chocolate", "Шоколад", "Al Fakher", "500", "alFakher/R2_50g_Carton_Chocolate_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Chocolate lovers beware; we've managed to reproduce the enticing palate of fine chocolate in our molasses. The powerful taste of cocoa with a beautiful creaminess.", "Любители шоколада, внимание! В нашей смеси мы постарались воссоздать восхитительный вкус роскошного шоколада. Яркий аромат какао и мягкие сливочные ноты.", "Chocolate", "Шоколад", "Al Fakher", "50", "alFakher/R2_50g_Pack_Chocolate_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Flavoured with the essence of petals, rose’s soothing taste combined with its fragrance is a real pleasure for all your senses", "Вкус смеси с маслом розовых лепестков расслабляет, а в сочетании с ароматом дарит наслаждение всем органам чувств.", "Rose", "Роза", "Al Fakher", "1000", "alFakher/Rose_1KG_Pack_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Flavoured with the essence of petals, rose’s soothing taste combined with its fragrance is a real pleasure for all your senses", "Вкус смеси с маслом розовых лепестков расслабляет, а в сочетании с ароматом дарит наслаждение всем органам чувств.", "Rose", "Роза", "Al Fakher", "250", "alFakher/Rose_250g_Pack_NEW_01_copy.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Flavoured with the essence of petals, rose’s soothing taste combined with its fragrance is a real pleasure for all your senses", "Вкус смеси с маслом розовых лепестков расслабляет, а в сочетании с ароматом дарит наслаждение всем органам чувств.", "Rose", "Роза", "Al Fakher", "500", "alFakher/R2_50g_Carton_Rose_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Flavoured with the essence of petals, rose’s soothing taste combined with its fragrance is a real pleasure for all your senses", "Вкус смеси с маслом розовых лепестков расслабляет, а в сочетании с ароматом дарит наслаждение всем органам чувств.", "Rose", "Роза", "Al Fakher", "50", "alFakher/R2_50g_Pack_Rose_copy_1.png",112.0));
            tobaccoEntities.add(new TobaccoEntity("STANDARD RANGE", "Стандартная коллекция", "Cinnamon’s unique taste and aroma will be a treat for you and your friends. A mix of sugar, spice and everything nice.", "Уникальный вкус и аромат корицы порадует вас и ваших друзей. Смесь ноток сахара, специй и приятных ощущений.", "Cinnamon", "Корица", "Al Fakher", "1000", "alFakher/Cinnamon_1KG_Pack_copy.png",112.0));
            for (TobaccoEntity tobaccoEntity :
                    tobaccoEntities) {
                tobaccoRepository.save(tobaccoEntity);
            }*/
        });
    }
}
